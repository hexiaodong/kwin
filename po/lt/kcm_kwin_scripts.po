# Lithuanian translations for l package.
# Copyright (C) 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
#
# Automatically generated, 2012.
# Liudas Ališauskas <liudas.alisauskas@gmail.com>, 2012, 2015.
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-21 02:02+0000\n"
"PO-Revision-Date: 2022-12-12 21:28+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.2.2\n"

#: module.cpp:50
#, kde-format
msgid "Import KWin Script"
msgstr "Importuoti KWin scenarijų"

#: module.cpp:51
#, kde-format
msgid "*.kwinscript|KWin scripts (*.kwinscript)"
msgstr "*.kwinscript|KWin scenarijai (*.kwinscript)"

#: module.cpp:62
#, kde-format
msgctxt "Placeholder is error message returned from the install service"
msgid ""
"Cannot import selected script.\n"
"%1"
msgstr ""
"Nepavyksta importuoti pasirinkto scenarijaus.\n"
"%1"

#: module.cpp:66
#, kde-format
msgctxt "Placeholder is name of the script that was imported"
msgid "The script \"%1\" was successfully imported."
msgstr "Scenarijus \"%1\" buvo sėkmingai importuotas."

#: module.cpp:125
#, kde-format
msgid "Error when uninstalling KWin Script: %1"
msgstr "Klaida šalinant KWin scenarijų: %1"

#: ui/main.qml:23
#, fuzzy, kde-format
#| msgid "Install from File..."
msgid "Install from File…"
msgstr "Įdiegti iš failo..."

#: ui/main.qml:27
#, fuzzy, kde-format
#| msgid "Get New Scripts..."
msgctxt "@action:button get new KWin scripts"
msgid "Get New…"
msgstr "Gauti naujų scenarijų..."

#: ui/main.qml:65
#, fuzzy, kde-format
#| msgctxt "@info:tooltip"
#| msgid "Delete..."
msgctxt "@info:tooltip"
msgid "Delete…"
msgstr "Ištrinti..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Liudas Ališauskas, Moo"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "liudas@akmc.lt, <>"

#~ msgid "KWin Scripts"
#~ msgstr "KWin scenarijai"

#~ msgid "Configure KWin scripts"
#~ msgstr "Konfigūruoti KWin scenarijus"

#~ msgid "Tamás Krutki"
#~ msgstr "Tamás Krutki"

#~ msgid "KWin script configuration"
#~ msgstr "KWin scenarijaus konfigūracija"

#~ msgid "Import KWin script..."
#~ msgstr "Importuoti KWin scenarijų..."

#~ msgid ""
#~ "Cannot import selected script: maybe a script already exists with the "
#~ "same name or there is a permission problem."
#~ msgstr ""
#~ "Negalima importuoti pasirinkto scenarijaus: galbūt scenarijus jau yra su "
#~ "tokiu pačiu pavadinimu arba yra leidimų problema."
